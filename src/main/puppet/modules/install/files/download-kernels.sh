#!/usr/bin/env bash

if [ $(/usr/bin/id -u) -ne 0 ]; then
    echo "Not running as root or using sudo"
    exit 1
fi

DEBIAN_WHEEZY_KERNEL_DOWNLOADER=/srv/tftp/debian/7-wheezy/kernels.netboot.sh
bash ${DEBIAN_WHEEZY_KERNEL_DOWNLOADER}

XENSERVER_KERNEL_DOWNLOADER=/srv/tftp/xenserver/kernels.sh
bash ${XENSERVER_KERNEL_DOWNLOADER}
