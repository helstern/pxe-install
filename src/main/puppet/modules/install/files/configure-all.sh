#!/usr/bin/env bash

if [ $(/usr/bin/id -u) -ne 0 ]; then
    echo "Not running as root or using sudo"
    exit 1
fi

CONF_SOURCE_DIR=$( cd "$( dirname "${BASH_SOURCE[0]}" )" && cd ../../conf/files && pwd )
# copy conf files
cp -rf ${CONF_SOURCE_DIR}/* /

# copy pxe boot files
cp -f /usr/lib/syslinux/pxelinux.0 /srv/tftp
cp -f /usr/lib/syslinux/vesamenu.c32 /srv/tftp
cp -f /usr/lib/syslinux/menu.c32 /srv/tftp

# restart services
service dnsmasq restart
service tftpd-hpa restart

# install the pxe http site
a2ensite pxe-install
service apache2 reload
