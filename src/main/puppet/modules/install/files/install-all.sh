#!/usr/bin/env bash

    sudo apt-get install dnsmasq tftpd-hpa apache2
    # to have to pxe boot files
    sudo apt-get install syslinux
    # for mounting windows shares and windows dns resolution
    sudo apt-get install -y winbind libnss-winbind

    sudo mkdir -p /var/log/dnsmasq
    sudo chown -R dnsmasq /var/log/dnsmasq

    sudo touch /var/log/tftpd
