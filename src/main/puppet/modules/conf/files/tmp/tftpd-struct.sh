#!/usr/bin/env bash

mkdir -p /srv/tftp/linux/xen/6.5
mkdir -p /srv/tftp/pxelinux.cfg

cat <<PXE_DEFAULT > /srv/tftp/pxelinux.cfg/default

PXE_DEFAULT

cat <<BOOT.TXT > /srv/tftp/boot.txt

BOOT.TXT


chmod -R 777 /srv/tftp
chown -R nobody:nogroup /srv/tftp
