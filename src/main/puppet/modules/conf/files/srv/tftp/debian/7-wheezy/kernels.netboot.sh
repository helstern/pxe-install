#!/usr/bin/env bash

# check that script is running with elevated privileges
if [ $(/usr/bin/id -u) -ne 0 ]; then
    echo "Not running as root or using sudo"
    exit 1
fi

PXE_ROOT_DIR=$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )
# this file needs to be parametrized by architecture type and installer type

MIRROR=http://http.debian.net/debian/dists/stable/main/installer-amd64/current/images/netboot/debian-installer/amd64
FILENAME=linux
wget --output-document=${PXE_ROOT_DIR}/amd64/${FILENAME} ${MIRROR}/${FILENAME}

FILENAME=initrd.gz
wget --output-document=${PXE_ROOT_DIR}/amd64/${FILENAME} ${MIRROR}/${FILENAME}
