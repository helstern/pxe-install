#!/usr/bin/env bash

# check that script is running with elevated privileges
if [ $(/usr/bin/id -u) -ne 0 ]; then
    echo "Not running as root or using sudo"
    exit 1
fi

PXE_ROOT_DIR=$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )

cp -f /usr/lib/syslinux/mboot.c32 ${PXE_ROOT_DIR}/
