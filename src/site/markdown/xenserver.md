
xenserver is based on centos

resources

- http://juliankessel.de/content/plugins/really-static/static/2014/07/18/configuring-xenserver-centos-to-send-hostname-on-dhcp/

list hosts
    
    xe host-list
    
change hostname - on xen host

run  

    ifconfig

to get information about the name and the mac of the dhcp enabled interface. Then
 

    HOST_NAME=xen-one; HOST_UUID=$(xe host-list params=uuid | head -n 1 | cut -f2 -d ':' | tr -d ' ')
    xe host-set-hostname-live host-uuid=$HOST_UUID host-name=$HOST_NAME
    
     cat <<FILE /etc/sysconfig/network-scripts/ifcfg-xenbr0
        DEVICE=<nic name obtained from ifconfig>
        BOOTPROTO=dhcp
        HWADDR=<Hardware Address obtained from ifconfig>
        ONBOOT=yes
        DHCP_HOSTNAME=$HOST_NAME     
     FILE
    
    service network restart
    
add ssh key - on client
    
    HOST_NAME=xen-one; scp /home/$USER/.ssh/id_rsa.pub root@$HOST_NAME:/tmp

on host   
 
    cat /tmp/id_rsa.pub >> /root/.ssh/authorized_keys
    chmod 600 /root/.ssh/authorized_keys 
    
