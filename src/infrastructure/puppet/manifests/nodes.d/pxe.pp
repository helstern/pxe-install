node 'pxe.local' {
  # setting default path for executable resources
  Exec {
    path => ['/usr/bin', '/bin', '/usr/sbin', '/sbin', '/usr/local/bin', '/usr/local/sbin']
  }

  include setup_network
}
