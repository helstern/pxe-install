Vagrant.configure(VAGRANTFILE_API_VERSION) do |config|

  unless $box_configuration.fetch(:fileserver_mount_src).to_s.empty?
    config.vm.synced_folder $box_configuration.fetch(:fileserver_mount_src), '/puppet/puppet-fileserver', {}
    config.vm.provision :shell, inline: 'bash /puppet/puppet-fileserver/bin/download.sh'
  end

end
